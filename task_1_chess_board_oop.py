"""
Task 1. After input 2 parameters you can see chess board
"""


class UsersInput:
    def __init__(self, prompt):
        self._prompt = prompt

    def value(self) -> str:
        return input(self._prompt)


class Validator:
    def input_size(self, input: UsersInput) -> int:
        while True:
            try:
                inputted = abs(int(input.value()))
                if inputted == 0:
                    raise ValueError
                return inputted
            except ValueError:
                print("Please, input digit more than zero!")


class ChessBoard:
    def __init__(self, height: int, width: int):
        self.height = height
        self.width = width

    def print_chess_board(self) -> None:
        height_and_width_set = [['*'] * self.width for column_number in range(self.height)]
        for column_number in range(self.height):
            for line_number in range(self.width):
                if (column_number + line_number) % 2 == 0:
                    height_and_width_set[column_number][line_number] = ' '
        for row in height_and_width_set:
            print(' '.join([elem for elem in row]))


class AskUser:
    def you_can_create_new_chess_board(self) -> bool:
        user_answer = input("Do you want to create new chess board? [y / yes]\n")
        if (user_answer.lower() == "y") or (user_answer.lower() == "yes"):
            return True
        else:
            print("Thanks!")
            return False


if __name__ == '__main__':
        while True:
            inputted_height = Validator().input_size(UsersInput("Input height: "))
            inputted_width = Validator().input_size(UsersInput("Input width: "))
            chess = ChessBoard(inputted_height, inputted_width)
            chess.print_chess_board()
            if not AskUser().you_can_create_new_chess_board():
                break
