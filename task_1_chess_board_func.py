"""
Task 1. After input 2 parameters you can see chess board
"""


def input_size(msg: str) -> int:
    while True:
        try:
            inputted = abs(int(input(msg)))
            if inputted == 0:
                raise ValueError
            return inputted
        except ValueError:
            print("Please, input digit!")


def print_chess_board(height:int, width:int) -> None:
    height_and_width_set = [['*'] * width for column_number in range(height)]
    for column_number in range(height):
        for line_number in range(width):
            if (column_number + line_number) % 2 == 0:
                height_and_width_set[column_number][line_number] = ' '
    for row in height_and_width_set:
        print(' '.join([elem for elem in row]))


def ask_user() -> bool:
    user_answer = input("Do you want to create new chess board? [y / yes]\n")
    if (user_answer.lower() == "y") or (user_answer.lower() == "yes"):
        return True
    else:
        print("Thanks!")
        return False


def main():
    while True:
        inputted_height = (input_size("Input height: "))
        inputted_width = (input_size("Input width: "))
        print_chess_board(inputted_height, inputted_width)
        if not ask_user():
            break


if __name__ == '__main__':
    main()
